class MiBarra extends HTMLElement {
    constructor () {
        const tpl = document.querySelector('template#tplbarra');
        const tplInst = tpl.content.cloneNode(true);

        super();
        this.attachShadow({mode:'open'});
        this.shadowRoot.appendChild(tplInst);

    }
}
customElements.define('mi-barra',MiBarra);

class MiTabla extends HTMLElement {
    constructor () {
        const tpl = document.querySelector('template#tpltabla');
        const tplInst = tpl.content.cloneNode(true);

        super();
        this.attachShadow({mode:'open'});
        this.shadowRoot.appendChild(tplInst);

    }
}

customElements.define('mi-tabla',MiTabla);


// console.log(baseElement.content.querySelector("find"));
console.log(document.querySelector("mi-tabla").shadowRoot);
var datas
var baseElement = document.querySelector("mi-tabla").shadowRoot.querySelector("mi-barra").shadowRoot;
var baseElement2 = document.querySelector("mi-tabla").shadowRoot;

var latabla = baseElement2.querySelector("table");

baseElement.querySelector("#find").addEventListener('click', function(e){
     fetch('http://dummy.restapiexample.com/api/v1/employees')
     .then(response => response.json())
     .then(data => { datas = data });

     while (latabla.hasChildNodes()) {  
        latabla.removeChild(latabla.firstChild);
      }
    console.log(baseElement.querySelector("#inputBusca").value);

    for (var i = 0; i < datas["data"].length; i ++) {
        mitr = document.createElement("tr");

        mitd1 = document.createElement("td");
        mitd1.textContent = datas["data"][i]["id"];
        mitr.appendChild(mitd1);
        mitd2 = document.createElement("td");
        mitd2.textContent = datas["data"][i]["employee_name"];
        mitr.appendChild(mitd2);

        if( datas["data"][i]["employee_name"].includes(baseElement.querySelector("#inputBusca").value) ) {
            latabla.appendChild(mitr);
        }
    }
    console.log(latabla);
}); 

baseElement.querySelector("#clear").addEventListener('click', function(e){

    while (latabla.hasChildNodes()) {  
        latabla.removeChild(latabla.firstChild);
    }
});
